package ru.t1.malyugin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.dto.request.system.ServerAboutRequest;
import ru.t1.malyugin.tm.dto.request.system.ServerInfoRequest;
import ru.t1.malyugin.tm.dto.request.system.ServerVersionRequest;
import ru.t1.malyugin.tm.dto.response.system.ServerAboutResponse;
import ru.t1.malyugin.tm.dto.response.system.ServerInfoResponse;
import ru.t1.malyugin.tm.marker.SoapCategory;

import static ru.t1.malyugin.tm.TestData.SYSTEM_ENDPOINT;

@Category(SoapCategory.class)
public final class SystemEndpointTest {

    @Test
    public void testGetVersion() {
        @NotNull final ServerVersionRequest serverVersionRequest = new ServerVersionRequest();
        Assert.assertNotNull(SYSTEM_ENDPOINT.getVersion(serverVersionRequest).getVersion());
    }

    @Test
    public void testGetAbout() {
        @NotNull final ServerAboutRequest serverAboutRequest = new ServerAboutRequest();
        @NotNull final ServerAboutResponse serverAboutResponse = SYSTEM_ENDPOINT.getAbout(serverAboutRequest);
        Assert.assertNotNull(serverAboutResponse.getGitMessage());
        Assert.assertNotNull(serverAboutResponse.getGitBranch());
        Assert.assertNotNull(serverAboutResponse.getGitCommitId());
        Assert.assertNotNull(serverAboutResponse.getGitCommitterEmail());
        Assert.assertNotNull(serverAboutResponse.getGitCommitterName());
        Assert.assertNotNull(serverAboutResponse.getGitTime());
        Assert.assertNotNull(serverAboutResponse.getServerName());
        Assert.assertNotNull(serverAboutResponse.getAuthorEmail());
        Assert.assertNotNull(serverAboutResponse.getAuthorName());
    }

    @Test
    public void testGetInfo() {
        @NotNull final ServerInfoRequest serverInfoRequest = new ServerInfoRequest();
        @NotNull final ServerInfoResponse serverInfoResponse = SYSTEM_ENDPOINT.getInfo(serverInfoRequest);
        Assert.assertNotNull(serverInfoResponse.getFreeMemory());
        Assert.assertNotNull(serverInfoResponse.getMaxMemory());
        Assert.assertNotNull(serverInfoResponse.getProcessorsCount());
        Assert.assertNotNull(serverInfoResponse.getTotalMemory());
        Assert.assertNotNull(serverInfoResponse.getUsedMemory());
    }

}