package ru.t1.malyugin.tm;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.repository.IProjectRepository;
import ru.t1.malyugin.tm.api.repository.ISessionRepository;
import ru.t1.malyugin.tm.api.repository.ITaskRepository;
import ru.t1.malyugin.tm.api.repository.IUserRepository;
import ru.t1.malyugin.tm.api.service.*;
import ru.t1.malyugin.tm.component.ServiceLocator;
import ru.t1.malyugin.tm.dto.model.ProjectDTO;
import ru.t1.malyugin.tm.dto.model.SessionDTO;
import ru.t1.malyugin.tm.dto.model.TaskDTO;
import ru.t1.malyugin.tm.dto.model.UserDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public interface TestData {

    int NUMBER_OF_PROJECTS = 10;

    int NUMBER_OF_TASKS = 10;

    int NUMBER_OF_USERS = 10;

    int NUMBER_OF_SESSION = 10;

    @NotNull
    String FIRST_USUAL_USER_LOGIN = "TST_USER_1";

    @NotNull
    String FIRST_USUAL_USER_PASS = "TST_PASS_1";

    @NotNull
    String SECOND_USUAL_USER_LOGIN = "TST_USER_2";

    @NotNull
    String SECOND_USUAL_USER_PASS = "TST_PASS_2";

    @NotNull
    String UNKNOWN_ID = UUID.randomUUID().toString();

    @NotNull
    String UNKNOWN_STRING = "SOME_STR";

    @NotNull
    List<ProjectDTO> PROJECT_LIST = new ArrayList<>();

    @NotNull
    List<TaskDTO> TASK_LIST = new ArrayList<>();

    @NotNull
    List<UserDTO> USER_LIST = new ArrayList<>();

    @NotNull
    List<SessionDTO> SESSION_LIST = new ArrayList<>();

    @NotNull
    IServiceLocator SERVICE_LOCATOR = new ServiceLocator();

    @NotNull
    IPropertyService PROPERTY_SERVICE = SERVICE_LOCATOR.getPropertyService();

    @NotNull
    ISessionService SESSION_SERVICE = SERVICE_LOCATOR.getSessionService();

    @NotNull
    ITaskService TASK_SERVICE = SERVICE_LOCATOR.getTaskService();

    @NotNull
    IProjectService PROJECT_SERVICE = SERVICE_LOCATOR.getProjectService();

    @NotNull
    IUserService USER_SERVICE = SERVICE_LOCATOR.getUserService();

    @NotNull
    IConnectionService CONNECTION_SERVICE = SERVICE_LOCATOR.getConnectionService();

    @NotNull
    SqlSession SQL_SESSION = CONNECTION_SERVICE.getSqlSession(true);

    @NotNull
    IProjectRepository PROJECT_REPOSITORY = SQL_SESSION.getMapper(IProjectRepository.class);

    @NotNull
    ITaskRepository TASK_REPOSITORY = SQL_SESSION.getMapper(ITaskRepository.class);

    @NotNull
    IUserRepository USER_REPOSITORY = SQL_SESSION.getMapper(IUserRepository.class);

    @NotNull
    ISessionRepository SESSION_REPOSITORY = SQL_SESSION.getMapper(ISessionRepository.class);

}