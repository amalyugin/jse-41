package ru.t1.malyugin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.dto.model.ProjectDTO;
import ru.t1.malyugin.tm.dto.model.TaskDTO;
import ru.t1.malyugin.tm.dto.model.UserDTO;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.marker.UnitCategory;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static ru.t1.malyugin.tm.TestData.*;

@Category(UnitCategory.class)
public class TaskRepositoryTest {

    @NotNull
    private static UserDTO user1 = new UserDTO();

    @NotNull
    private static UserDTO user2 = new UserDTO();

    @BeforeClass
    public static void initUser() throws Exception {
        user1 = USER_SERVICE.create(FIRST_USUAL_USER_LOGIN, FIRST_USUAL_USER_PASS, null, Role.USUAL);
        user2 = USER_SERVICE.create(SECOND_USUAL_USER_LOGIN, SECOND_USUAL_USER_PASS, null, Role.USUAL);
    }

    @AfterClass
    public static void clearUser() {
        USER_SERVICE.removeById(user1.getId());
        USER_SERVICE.removeById(user2.getId());
    }

    @Before
    public void initRepository() {
        @NotNull final ProjectDTO project1 = PROJECT_SERVICE.create(user1.getId(), "P_1", "D_1");
        @NotNull final ProjectDTO project2 = PROJECT_SERVICE.create(user1.getId(), "P_2", "D_2");

        for (int i = 1; i <= NUMBER_OF_TASKS; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Task N " + i);
            task.setDescription("Desc " + i);
            if (i <= NUMBER_OF_TASKS / 2) {
                task.setUserId(user1.getId());
                task.setProjectId(project1.getId());
            } else {
                task.setUserId(user2.getId());
                task.setProjectId(project2.getId());
            }
            TASK_REPOSITORY.add(task);
            TASK_LIST.add(task);
        }
    }

    @After
    public void clearData() {
        TASK_SERVICE.clear();
        PROJECT_SERVICE.clear();
        TASK_LIST.clear();
        PROJECT_LIST.clear();
    }

    @Test
    public void testFindAllByProjectId() {
        @NotNull final List<TaskDTO> actualTaskList = TASK_REPOSITORY.findAll(user1.getId());
        @NotNull final String tempProjectId = "TEMP_PROJECT_ID";
        actualTaskList.get(0).setProjectId(tempProjectId);
        actualTaskList.get(1).setProjectId(tempProjectId);
        actualTaskList.get(2).setProjectId(tempProjectId);

        @NotNull final List<TaskDTO> expectedTaskList = TASK_LIST
                .stream()
                .filter(p -> user1.getId().equals(p.getUserId()))
                .filter(p -> tempProjectId.equals(p.getProjectId()))
                .collect(Collectors.toList());
        Assert.assertEquals(expectedTaskList, TASK_REPOSITORY.findAllByProjectId(user1.getId(), tempProjectId));
        Assert.assertEquals(Collections.EMPTY_LIST, TASK_REPOSITORY.findAllByProjectId(user2.getId(), tempProjectId));
    }

    @Test
    public void testUpdate() {
        for (@NotNull final TaskDTO task : TASK_LIST) {
            @NotNull final String newName = "NEW TASK";
            @NotNull final String newDescription = "NEW DESCRIPTION";
            @NotNull final Status newStatus = Status.COMPLETED;
            @Nullable final String newProjectId = null;
            task.setName(newName);
            task.setDescription(newDescription);
            task.setStatus(newStatus);
            task.setProjectId(newProjectId);
            TASK_REPOSITORY.update(task);
            @Nullable final TaskDTO newTask = TASK_REPOSITORY.findOneById(task.getId());
            Assert.assertNotNull(newTask);
            Assert.assertEquals(newName, newTask.getName());
            Assert.assertEquals(newDescription, newTask.getDescription());
            Assert.assertEquals(newStatus, newTask.getStatus());
            Assert.assertEquals(newProjectId, newTask.getProjectId());
        }
    }

}