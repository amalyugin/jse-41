package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.model.TaskDTO;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.enumerated.Status;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    void clear();

    void clear(
            @Nullable String userId
    );

    void removeById(
            @Nullable String userId,
            @Nullable String id
    );

    void removeByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @Nullable
    TaskDTO findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    TaskDTO findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @NotNull
    List<TaskDTO> findAll(
            @Nullable String userId
    );

    @NotNull
    List<TaskDTO> findAll(
            @Nullable String userId,
            @Nullable Sort sort
    );

    @NotNull
    @SuppressWarnings("rawtypes")
    List<TaskDTO> findAll(
            @Nullable String userId,
            @Nullable Comparator comparator
    );

    int getSize();

    @NotNull
    int getSize(
            @Nullable String userId
    );

    @NotNull
    TaskDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    TaskDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    TaskDTO updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    TaskDTO changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    TaskDTO changeTaskStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    );

    @NotNull
    TaskDTO bindTaskToProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    );

    @NotNull
    TaskDTO unbindTaskFromProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    );

    @NotNull
    List<TaskDTO> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    );

}