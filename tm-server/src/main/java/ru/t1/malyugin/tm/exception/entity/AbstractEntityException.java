package ru.t1.malyugin.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.exception.AbstractException;

public abstract class AbstractEntityException extends AbstractException {

    public AbstractEntityException() {
    }

    public AbstractEntityException(@NotNull final String message) {
        super(message);
    }

    public AbstractEntityException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractEntityException(@NotNull final String message, @NotNull final Throwable cause,
                                   final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}