package ru.t1.malyugin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.endpoint.IUserEndpoint;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.api.service.IUserService;
import ru.t1.malyugin.tm.dto.model.SessionDTO;
import ru.t1.malyugin.tm.dto.model.UserDTO;
import ru.t1.malyugin.tm.dto.request.user.*;
import ru.t1.malyugin.tm.dto.response.user.*;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.exception.server.EndpointException;
import ru.t1.malyugin.tm.exception.user.UserNotFoundException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.malyugin.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    @WebMethod
    public UserChangePasswordResponse changePassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserChangePasswordRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String password = request.getPassword();
        @Nullable final UserDTO user;
        try {
            user = getUserService().setPassword(userId, password);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new UserChangePasswordResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLockResponse lockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLockRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final UserDTO user;
        try {
            user = getUserService().lockUserByLogin(login);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new UserLockResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserUnlockResponse unlockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUnlockRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final UserDTO user;
        try {
            user = getUserService().unlockUserByLogin(login);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new UserUnlockResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserRemoveResponse removeUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRemoveRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final UserDTO user;
        try {
            user = getUserService().removeByLogin(login);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new UserRemoveResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserUpdateProfileResponse updateProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUpdateProfileRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String middleName = request.getMiddleName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final UserDTO user;
        try {
            user = getUserService().updateProfile(userId, firstName, lastName, middleName);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new UserUpdateProfileResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserGetProfileResponse getProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserGetProfileRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final UserDTO user;
        try {
            user = getUserService().findOneById(userId);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        if (user == null) throw new UserNotFoundException();
        return new UserGetProfileResponse(user);
    }

}