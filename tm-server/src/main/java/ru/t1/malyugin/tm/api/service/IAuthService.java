package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.model.SessionDTO;

public interface IAuthService {

    @NotNull
    String registry(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    ) throws Exception;

    @NotNull
    String login(
            @Nullable String login,
            @Nullable String password
    ) throws Exception;

    @NotNull
    SessionDTO validateToken(
            @Nullable String token
    );

    void invalidate(
            @Nullable SessionDTO session
    ) throws Exception;

}