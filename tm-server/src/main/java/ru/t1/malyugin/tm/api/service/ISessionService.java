package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.model.SessionDTO;

public interface ISessionService {

    void clear();

    void clear(@Nullable String userId);

    void remove(@Nullable SessionDTO session);

    @Nullable
    SessionDTO findOneById(@Nullable String id);

    void add(@Nullable SessionDTO session);

}