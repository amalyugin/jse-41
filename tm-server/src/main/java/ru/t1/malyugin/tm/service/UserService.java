package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.repository.IProjectRepository;
import ru.t1.malyugin.tm.api.repository.ISessionRepository;
import ru.t1.malyugin.tm.api.repository.ITaskRepository;
import ru.t1.malyugin.tm.api.repository.IUserRepository;
import ru.t1.malyugin.tm.api.service.IConnectionService;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.api.service.IUserService;
import ru.t1.malyugin.tm.dto.model.UserDTO;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.exception.field.*;
import ru.t1.malyugin.tm.exception.user.EmailExistException;
import ru.t1.malyugin.tm.exception.user.LoginExistException;
import ru.t1.malyugin.tm.exception.user.UserNotFoundException;
import ru.t1.malyugin.tm.util.HashUtil;

import java.util.Collections;
import java.util.List;

public final class UserService implements IUserService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public UserService(
            @NotNull final IServiceLocator serviceLocator
    ) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    private IConnectionService getConnectionService() {
        return serviceLocator.getConnectionService();
    }

    private void updateUser(@NotNull final UserDTO user) {
        @NotNull final SqlSession session = getConnectionService().getSqlSession(false);
        try {
            @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
            userRepository.update(user);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    private void addUser(@NotNull final UserDTO user) {
        @NotNull final SqlSession session = getConnectionService().getSqlSession(false);
        try {
            @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
            userRepository.add(user);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    private void removeUser(@NotNull final UserDTO user) {
        @NotNull final SqlSession session = getConnectionService().getSqlSession(false);
        try {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @NotNull final ISessionRepository sessionRepository = session.getMapper(ISessionRepository.class);
            @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
            taskRepository.clearForUser(user.getId());
            projectRepository.clearForUser(user.getId());
            sessionRepository.clearForUser(user.getId());
            userRepository.remove(user);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final SqlSession sqlSession = getConnectionService().getSqlSession(false);
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (StringUtils.isBlank(id)) throw new UserIdEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        removeUser(user);
    }

    @Nullable
    @Override
    public UserDTO findOneById(
            @Nullable final String id) {
        if (StringUtils.isBlank(id)) throw new ProjectIdEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
            return userRepository.findOneById(id);
        }
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
            @Nullable final List<UserDTO> users = userRepository.findAll();
            if (users == null) return Collections.emptyList();
            return users;
        }
    }

    @Override
    public int getSize() {
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
            @Nullable final Integer size = userRepository.getSize();
            if (size == null) return 0;
            return size;
        }
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        if (StringUtils.isBlank(password)) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (!StringUtils.isBlank(email) && isEmailExist(email)) throw new EmailExistException();
        @Nullable final String passwordHash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (passwordHash == null) throw new PasswordEmptyException();

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        if (!StringUtils.isBlank(email)) user.setEmail(email.trim());
        if (role != null) user.setRole(role);
        addUser(user);
        return user;
    }

    @Override
    public UserDTO lockUserByLogin(@Nullable final String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        @Nullable final UserDTO user = findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        updateUser(user);
        return user;
    }

    @Override
    public UserDTO unlockUserByLogin(@Nullable final String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        @Nullable final UserDTO user = findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        updateUser(user);
        return user;
    }

    @NotNull
    @Override
    public UserDTO removeByLogin(@Nullable final String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        @Nullable final UserDTO user = findOneByLogin(login.trim());
        if (user == null) throw new UserNotFoundException();
        removeUser(user);
        return user;
    }

    @NotNull
    @Override
    public UserDTO removeByEmail(@Nullable final String email) {
        if (StringUtils.isBlank(email)) throw new EmailEmptyException();
        @Nullable final UserDTO user = findOneByEmail(email.trim());
        if (user == null) throw new UserNotFoundException();
        removeUser(user);
        return user;
    }

    @NotNull
    @Override
    public UserDTO setPassword(@Nullable final String id, @Nullable final String password) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        if (StringUtils.isBlank(password)) throw new PasswordEmptyException();
        @Nullable final UserDTO user = findOneById(id.trim());
        if (user == null) throw new UserNotFoundException();
        @Nullable final String passwordHash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (passwordHash == null) throw new PasswordEmptyException();
        user.setPasswordHash(passwordHash);
        updateUser(user);
        return user;
    }

    @NotNull
    @Override
    public UserDTO updateProfile(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final UserDTO user = findOneById(id.trim());
        if (user == null) throw new UserNotFoundException();
        if (!StringUtils.isBlank(firstName)) user.setFirstName(firstName.trim());
        if (!StringUtils.isBlank(lastName)) user.setLastName(lastName.trim());
        if (!StringUtils.isBlank(middleName)) user.setMiddleName(middleName.trim());
        updateUser(user);
        return user;
    }

    @Nullable
    @Override
    public UserDTO findOneByLogin(@Nullable final String login) {
        if (StringUtils.isBlank(login)) return null;
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
            return userRepository.findOneByLogin(login.trim());
        }
    }

    @Nullable
    @Override
    public UserDTO findOneByEmail(@Nullable final String email) {
        if (StringUtils.isBlank(email)) return null;
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession(false)) {
            @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
            return userRepository.findOneByEmail(email.trim());
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (StringUtils.isBlank(login)) return false;
        return findOneByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (StringUtils.isBlank(email)) return false;
        return findOneByEmail(email) != null;
    }

}