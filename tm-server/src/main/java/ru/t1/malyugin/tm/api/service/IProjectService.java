package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.model.ProjectDTO;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.enumerated.Status;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    void clear();

    void clear(
            @Nullable String userId
    );

    void removeById(
            @Nullable String userId,
            @Nullable String id
    );

    void removeByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @Nullable
    ProjectDTO findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    ProjectDTO findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @NotNull
    List<ProjectDTO> findAll(
            @Nullable String userId
    );

    @NotNull
    List<ProjectDTO> findAll(
            @Nullable String userId,
            @Nullable Sort sort
    );

    @NotNull
    @SuppressWarnings("rawtypes")
    List<ProjectDTO> findAll(
            @Nullable String userId,
            @Nullable Comparator comparator
    );

    int getSize();

    int getSize(
            @Nullable String userId
    );

    @NotNull
    ProjectDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    ProjectDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    ProjectDTO updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    ProjectDTO changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    @NotNull
    ProjectDTO changeProjectStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    );

}