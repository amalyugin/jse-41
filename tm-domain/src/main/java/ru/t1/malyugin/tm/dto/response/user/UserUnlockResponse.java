package ru.t1.malyugin.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.model.UserDTO;
import ru.t1.malyugin.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public class UserUnlockResponse extends AbstractResponse {

    @Nullable
    private UserDTO user;

    public UserUnlockResponse(@Nullable final UserDTO user) {
        this.user = user;
    }

}