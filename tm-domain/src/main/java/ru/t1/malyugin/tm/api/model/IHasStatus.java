package ru.t1.malyugin.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}